import React, { Component } from 'react';

class Results extends Component {


  render() {
    const rolls = this.props.rolls;
    let botchMessage, rollMessage, successMessage;
    if (!!this.props.error) {
      rollMessage = this.props.error;
    } else if (rolls.length === 0) {
      rollMessage = 'Heitä!';
    } else {
      const successes = this._countSuccesses(rolls, false);
      const damage = this._countSuccesses(rolls, true);
      const botches = successes === 0 ? this._countBotches(rolls) : false;
      botchMessage = botches > 0 ? `${botches} nopan botch!` : '';
      const diceString = rolls.length === 1 ? 'noppa' : 'noppaa';
      rollMessage = `Heitettiin ${rolls.length} ${diceString}.`;
      if (successes > 0) {
        const successString = successes === 1 ? 'onnistuminen' : 'onnistumista';
        let damageString;
        if (damage === 1) {
          damageString = `${damage} vahinko!`;
        } else {
          damageString = `${damage} vahinkoa!`;
        }
        successMessage = `${successes} ${successString}! ${damageString}`;
      } else {
        successMessage = 'Ei onnistumisia!';
      }
    }
    return (
      <section>
        {rollMessage} {successMessage} <span className="text-danger">{botchMessage}</span>
        <table className="table table-bordered table-condensed">
        <thead>
          <tr>
            <th>Heitto</th>
            <th>Tulos</th>
          </tr>
          </thead>
          <tbody>
        {rolls.map(roll =>
          <tr key={roll.key} className={this._getClassName(roll.roll)}>
            <td>{roll.key}</td>
            <td>{roll.roll}</td>
          </tr>)}
        </tbody>
        </table>
      </section>
    )
  }

  _countSuccesses(rolls, isDamage) {
    let successCount = 0;
    rolls.forEach(roll => {
      if (roll.roll >= 7) {
        successCount++;
      }
      if (!isDamage && roll.roll === 10) {
        successCount++;
      }
    });
    return successCount;
  }

  _countBotches(rolls) {
    let botches = 0;
    rolls.forEach(roll => {
      if (roll.roll === 1) {
        botches++;
      }
    });
    return botches;
  }

  _getClassName(roll) {
    if (roll === 1) {
      return 'danger';
    }
    if (roll >= 7 && roll !== 10) {
      return 'success';
    }
    if (roll === 10) {
      return 'info'
    }
  }
}
export default Results;
