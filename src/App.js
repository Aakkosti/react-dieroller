import React, { Component } from 'react';
import logo from './logo.svg';
import Results from './components/Results';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import _ from '../node_modules/lodash/lodash';

class App extends Component {

  constructor() {
    super();

    this.state = {
      dice: 1,
      rolls: [],
      error: null
    };
    this._handleChange = this._handleChange.bind(this);
    this._rollDice = this._rollDice.bind(this);
  }
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>React Dieroller</h2>
        </div>
        <form onSubmit={this._rollDice}>
          <label>
            Noppien määrä: <input value={this.state.dice} onChange={this._handleChange} type="tel" min="1" name="amount"></input>
          </label>
          <input type="submit" className="btn btn-primary"></input>
        </form>
        <Results rolls={this.state.rolls} error={this.state.error} />
      </div>
    );
  }

  _handleChange(event) {
    this.setState({
      dice: event.target.value
    });
  }

  _rollDice(event) {
    event.preventDefault();
    const invalidStringCheckRegex = /[^0-9+-]/g;
    const regex = /[+-]?[0-9]+/g;
    const inputString = _.isString(this.state.dice) ? this.state.dice : this.state.dice.toString();
    if (invalidStringCheckRegex.test(inputString)) {
      this._setErrorState();
      return;
    }
    let diceCount = 0;
    const matches = inputString.match(regex);
    if (matches === null) {
      this._setErrorState();
      return;
    }
    matches.forEach(match => {
      diceCount += parseInt(match, 10);
    });
    if (diceCount < 1) {
      diceCount = 0;
    }
    const newRolls = [];
    for (let i = 0; i < diceCount; i++) {
      newRolls.push(this._rollDie(i));
    }
    this.setState({
      rolls: newRolls,
      error: null
    });
  }

  _setErrorState() {
    this.setState({
      error: 'Virheellinen syöte.',
      rolls: []
    });
  }

  _rollDie(key) {
    const roll = Math.floor((Math.random() * 10) + 1);
    return {
      key: ++key,
      roll: roll
    };
  }
}

export default App;
